package views;

import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import controlers.FileMatch;

/**
 * This is the View class, that have the main method to lunch the application.
 * Its build using Java Swing elements and will interact with the controller, that will
 * perform the actual match.
 * @author Arturo Reyes
 *
 */
@SuppressWarnings("serial")
public class Main extends JPanel implements ActionListener, MouseListener {
  

  /**
 * The constructor that will instantiate each element of the interface and 
 * set initial values.
 */
public Main() {
    super(new BorderLayout());

    //Create the log first, because the action listeners
    //need to refer to it.
    log = new JTextArea(20, 20);
    log.setMargin(new Insets(5, 5, 5, 5));
    log.setEditable(false);
    JScrollPane logScrollPane = new JScrollPane(log);

    //Create a file choosers for each file.
    fcOne = new JFileChooser();
    fcTwo = new JFileChooser();

    //Adding the buttons to open the file selector.
    openButtonOne = new JButton("Open a File One...");
    openButtonOne.addActionListener(this);
    
    openButtonTwo = new JButton("Open a File Two...");
    openButtonTwo.addActionListener(this);
    
    //Adding the texfields that will show the path of each file.
    fileOneTF = new JTextField(20);
    fileOneTF.addActionListener(this);
    fileOneTF.addMouseListener(this);
    fileTwoTF = new JTextField(20);
    fileTwoTF.addActionListener(this);
    fileTwoTF.addMouseListener(this);
    
    //The compare button that will trigger the magic.
    compareButton = new JButton("Compare");
    compareButton.addActionListener(this);
    
    //The next four buttons are each one for a report.
    duplicatesButton = new JButton("Duplicates Report");
    duplicatesButton.addActionListener(this);
    duplicatesButton.setEnabled(false);
    
    noMatchButton = new JButton("No Matches Report");
    noMatchButton.addActionListener(this);
    noMatchButton.setEnabled(false);
    
    partialMatchButton = new JButton("Partial Matches by Transaction Report");
    partialMatchButton.addActionListener(this);
    partialMatchButton.setEnabled(false);
    
    partialByDateAmtButton = new JButton("Partial Matches by Date & Amt Report");
    partialByDateAmtButton.addActionListener(this);
    partialByDateAmtButton.setEnabled(false);

    //For layout purposes, put the buttons in a separate panel
    JPanel fileOnePanel = new JPanel(); //use FlowLayout
    fileOnePanel.add(fileOneTF);
    fileOnePanel.add(openButtonOne);
    JPanel fileTwoPanel = new JPanel(); //use FlowLayout
    fileTwoPanel.add(fileTwoTF);
    fileTwoPanel.add(openButtonTwo);
    
    JPanel filesPanel = new JPanel(); //use FlowLayout
    filesPanel.add(fileOnePanel, BorderLayout.NORTH);
    filesPanel.add(fileTwoPanel, BorderLayout.CENTER);    
    
    JPanel topPanel = new JPanel(); //use FlowLayout
    topPanel.add(filesPanel, BorderLayout.WEST);
    topPanel.add(compareButton, BorderLayout.EAST);   

    //Add the buttons and the log to this panel.   
    JPanel bottomPanel = new JPanel(); //use FlowLayout
    bottomPanel.add(partialMatchButton, BorderLayout.WEST);
    bottomPanel.add(noMatchButton, BorderLayout.EAST); 
    bottomPanel.add(duplicatesButton, BorderLayout.EAST);
    bottomPanel.add(partialByDateAmtButton, BorderLayout.EAST);
    
    
    add(topPanel,BorderLayout.NORTH);
    add(logScrollPane, BorderLayout.CENTER);
    add(bottomPanel, BorderLayout.SOUTH);    
    
  }

  public void actionPerformed(ActionEvent e) {

    //Handle open button action for File One.
    if (e.getSource() == openButtonOne || e.getSource() == fileOneTF) {
    	openDialogFileOne();
    	//Handle open button action for File Two.       
    } else if (e.getSource() == openButtonTwo || e.getSource() == fileTwoTF) 
    {
    	openDialogFileTwo(); 
    }else if (e.getSource() == compareButton) 
    {	
    	fileMatch = new FileMatch();
    	fileOne = fcOne.getSelectedFile();
    	fileTwo = fcTwo.getSelectedFile();
	  	if(validateFiles())
	  	{
	  		
	  		fileMatch.setFiles(fileOne,fileTwo);
	  		fileMatch.setLogArea(log);
	  		fileMatch.enableLog(false);
	  		if( fileMatch.matchFiles())
	  		{
	  			printMatchResults();   
	  			enableButtonReports();
	  		}
	  		
	  	}
		  
	}else if (e.getSource() == duplicatesButton) 
    {
		ReportTable dups = new ReportTable(ReportTable.DUPLICATES,fileMatch);
		dups.createAndShowGUI();
    }else if (e.getSource() == partialMatchButton) 
    {
		ReportTable partial = new ReportTable(ReportTable.PARTIAL_MATCH,fileMatch);
		partial.createAndShowGUI();
    }else if (e.getSource() == noMatchButton) 
    {
		ReportTable noMatch = new ReportTable(ReportTable.NO_MATCH,fileMatch);
		noMatch.createAndShowGUI();
    }else if (e.getSource() == partialByDateAmtButton) 
    {
		ReportTable dtAmt = new ReportTable(ReportTable.PARTIAL_DTAMT,fileMatch);
		dtAmt.createAndShowGUI();
    }
    
  }
  
  @Override
  public void mouseClicked(MouseEvent e) {
  	// TODO Auto-generated method stub
	  if(e.getSource() == fileOneTF) {
		  openDialogFileOne();  
	  } else if(e.getSource() == fileTwoTF) {
		  openDialogFileTwo();  
	  }   
  }

  @Override
  public void mousePressed(MouseEvent e) {
  	// NOT USED
  	
  }

  @Override
  public void mouseReleased(MouseEvent e) {
	// NOT USED
  	
  }

  @Override
  public void mouseEntered(MouseEvent e) {
	// NOT USED  
  	
  }

  @Override
  public void mouseExited(MouseEvent e) {
  	// NOT USED
  	
  }
  
  /**
 * Set and show the path of the file one.
 */
private void openDialogFileOne() 
  {
	  int returnVal = fcOne.showOpenDialog(Main.this);

      if (returnVal == JFileChooser.APPROVE_OPTION) {        
        //This is where a real application would open the file.
    	fileOne = fcOne.getSelectedFile();
        fileOneTF.setText(fileOne.getAbsolutePath());
      } else {
        log.append("Open command cancelled by user." + newline);
      }
      log.setCaretPosition(log.getDocument().getLength());
  }
  
  /**
 * Set and show the path of the file two.
 */
private void openDialogFileTwo() 
  {
	  int returnVal = fcTwo.showOpenDialog(Main.this);

      if (returnVal == JFileChooser.APPROVE_OPTION) {        
        //This is where a real application would open the file.   
    	fileTwo = fcOne.getSelectedFile();  
        fileTwoTF.setText(fileTwo.getAbsolutePath());
      } else {
        log.append("Open command cancelled by user." + newline);
      }
      log.setCaretPosition(log.getDocument().getLength()); 
  }
  
  
  
  /**
   * Perform a basic validation of each file. If they exist, if they are actual files
   * if they que be read and if they have the CSV extension.
 * @return true if it pass the validation or false if not. 
 */
private boolean validateFiles()
  {
	  boolean valid = true;
	  //Validate files still exist
		if(!fileOne.exists() ){
			log.append("File One does not longer exist." + newline);
			valid = false;
		}
		
		if(!fileTwo.exists() ){
			log.append("File Two does not longer exist." + newline);
			valid = false;
		}

		if(!fileOne.isFile() ||  !fileOne.canRead()){
			log.append("File One is not a valid file." + newline);
			valid = false;
		}
		
		if(!fileTwo.isFile() || !fileTwo.canRead()){
			log.append("File Two is not a valid file." + newline);
			valid = false;
		}
		
		if(!fileOne.getName().toUpperCase().endsWith(".CSV"))
		{
			log.append("File One does not have CSV extension." + newline);
			valid = false;
		}
		
		if(!fileTwo.getName().toUpperCase().endsWith(".CSV"))
		{
			log.append("File Two does not have CSV extension." + newline);
			valid = false;
		}
		
		return valid;
  }

  /**
   * Create the GUI and show it. For thread safety, this method should be
   * invoked from the event-dispatching thread.
   */
  private static void createAndShowGUI() {
    //Make sure we have nice window decorations.
    JFrame.setDefaultLookAndFeelDecorated(true);
    JDialog.setDefaultLookAndFeelDecorated(true);

    //Create and set up the window.
    JFrame frame = new JFrame("Tutuka Matching Demo");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    //Create and set up the content pane.
    JComponent newContentPane = new Main();
    newContentPane.setOpaque(true); //content panes must be opaque
    frame.setContentPane(newContentPane);

    //Display the window.
    frame.pack();
    frame.setVisible(true);
  }

  public static void main(String[] args) {
    //Schedule a job for the event-dispatching thread:
    //creating and showing this application's GUI.
    javax.swing.SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        createAndShowGUI();
      }
    });
  }
  
  /**
 * Method to display in the textArea the result of the matching.
 */
private void printMatchResults()
  {
	  	log.append("-------------------------------------------" + newline);
		log.append("Summary for File One 	: "+ fileOne.getName() + newline);		
		log.append("Total rows				: "+ fileMatch.getTotalRowsFileOne() + newline);
		log.append("Duplicates				: "+ fileMatch.getDuplicatesFileOne().size() + newline);
		log.append("Perfect matches			: "+ fileMatch.getPerfectMatch() + newline);
		log.append("Partial matches	by Id			: "+ fileMatch.getPartialMatch() + newline);
		log.append("Partial matches	by Date & Amt 			: "+ fileMatch.getMatchesByDateAmt().size() + newline);
		log.append("No matches				: "+ fileMatch.getNoMatch() + newline);
		log.append("-------------------------------------------" + newline);
		log.append("-------------------------------------------" + newline);
		log.append("Summary for File Two 	: "+ fileTwo.getName() + newline);		
		log.append("Total rows				: "+ fileMatch.getTotalRowsFileTwo() + newline);
		log.append("Duplicates				: "+ fileMatch.getDuplicatesFileTwo().size() + newline);
		log.append("Perfect matches			: "+ fileMatch.getPerfectMatch() + newline);
		log.append("Partial matches	by Id			: "+ fileMatch.getPartialMatch() + newline);
		log.append("Partial matches	by Date & Amt 			: "+ fileMatch.getMatchesByDateAmt().size() + newline);
		log.append("No matches				: "+ fileMatch.getUnMatchtedTransactionOnFileTwo().size() + newline);
		log.append("-------------------------------------------" + newline);
		
		
  }
  
  /**
 * Utility method to enable the buttons if there is any value to report.
 */
private void enableButtonReports() {
	  if(fileMatch.getDuplicatesFileOne().size() > 0)
			duplicatesButton.setEnabled(true);
		if(fileMatch.getPartialMatch()  > 0)
			partialMatchButton.setEnabled(true);
		if(fileMatch.getNoMatch() > 0 || fileMatch.getUnMatchtedTransactionOnFileTwo().size() > 0)
			noMatchButton.setEnabled(true);
		if(fileMatch.getNoMatchByDateAmt().size()  > 0)
			partialByDateAmtButton.setEnabled(true);
  }
  
  
  
  static private final String newline = "\n";
  private	JTextArea 		log;   
  private 	JFileChooser 	fcOne;
  private	JFileChooser 	fcTwo;
  private	JTextField 		fileOneTF;
  private	JTextField 		fileTwoTF;
  private	File 			fileOne;
  private	File 			fileTwo;    
  private	FileMatch		fileMatch;
  //Buttons
  private	JButton 		duplicatesButton;
  private	JButton 		noMatchButton;
  private	JButton 		partialMatchButton;
  private	JButton 		partialByDateAmtButton;
  private	JButton 		openButtonOne; 
  private	JButton 		openButtonTwo; 
  private	JButton 		compareButton;
}
