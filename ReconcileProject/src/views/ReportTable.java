package views;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import controlers.FileMatch;
import java.awt.Dimension;
import java.awt.GridLayout;

 
/**
 * This class will show a window with a table reporting the records that user wants
 * to see, the duplicates, no match, partial matches. 
 * It will receive a FileMatch object to get all that information.
 * @author Arturo Reyes
 *
 */
@SuppressWarnings("serial")
public class ReportTable extends JPanel {    
	
	/**
	 * Unique constructor, to set and define the table data.
	 * @param reportType - Type of report we are using:DUPLICATES, NO_MATCH, PARTIAL_MATCH and PARTIAL_DTAMT
	 * @param fileMatch - Object with the FileMatch information already set.
	 */
	public ReportTable(String reportType, FileMatch fileMatch) {
		super(new GridLayout(1,0));
        this.reportType	 = reportType;        
        this.fileMatch = fileMatch;
        
        buildData();
        buildTable();        
    }
    
    /**
     * Depending on the report type, it will construct the data set of the table. 
     */
    private void buildData()
    {
    	switch(reportType)
    	{
    		//The duplicates are being shown in pairs, since are records from the 
    		//same file.
    		case DUPLICATES:
    			int dupFO 		= fileMatch.getDuplicatesFileOne().size() ;
    			int dupFT 		= fileMatch.getDuplicatesFileTwo().size() ;
    			//Determine which file has more duplicates.
    			int rows 		= ( dupFO > dupFT ) ? dupFO : dupFT;
    			data 			= new Object[rows*2][6];
    			int dupRows 	= 0;
    			
    			for(int r = 0; r < rows*2; r+=2)
    			{    		
    				//Show the duplicates of the File One
    				if(r < dupFO*2)
    				{
    					int firstDuplicated = fileMatch.getDuplicatesFileOne().get(dupRows)[0];
    					int secondDuplicated = fileMatch.getDuplicatesFileOne().get(dupRows)[1];
    					//The date.
    					data[r][0] 		= fileMatch.getSpecificDataFromFile(fileMatch.getFileOneDateIndex(), FileMatch.FILE_ONE, firstDuplicated);
    					data[r+1][0] 	= fileMatch.getSpecificDataFromFile(fileMatch.getFileOneDateIndex(), FileMatch.FILE_ONE, secondDuplicated);
    					//The reference.
    					data[r][1] 		= fileMatch.getSpecificDataFromFile(fileMatch.getIndexTransactionFileOne(),FileMatch.FILE_ONE, firstDuplicated);
    					data[r+1][1] 	= fileMatch.getSpecificDataFromFile(fileMatch.getIndexTransactionFileOne(), FileMatch.FILE_ONE, secondDuplicated);
    					//The amount.
    					data[r][2] 		= fileMatch.getSpecificDataFromFile(fileMatch.getFileOneAmountIndex(), FileMatch.FILE_ONE, firstDuplicated);
    					data[r+1][2] 	= fileMatch.getSpecificDataFromFile(fileMatch.getFileOneAmountIndex(), FileMatch.FILE_ONE, secondDuplicated);
    				}
    				
    				//Show the duplicates of File Two.
    				if(r < dupFT*2)
    				{
    					int firstDuplicated = fileMatch.getDuplicatesFileTwo().get(dupRows)[0];
    					int secondDuplicated = fileMatch.getDuplicatesFileTwo().get(dupRows)[1];
    					//The date.
    					data[r][3] 		= fileMatch.getSpecificDataFromFile(fileMatch.getFileTwoDateIndex(), FileMatch.FILE_TWO, firstDuplicated);
    					data[r+1][3] 	= fileMatch.getSpecificDataFromFile(fileMatch.getFileTwoDateIndex(), FileMatch.FILE_TWO, secondDuplicated);
    					//The reference.
    					data[r][4] 		= fileMatch.getSpecificDataFromFile(fileMatch.getIndexTransactionFileTwo(), FileMatch.FILE_TWO, firstDuplicated);
    					data[r+1][4] 	= fileMatch.getSpecificDataFromFile(fileMatch.getIndexTransactionFileTwo(), FileMatch.FILE_TWO, secondDuplicated);
    					//The amount.
    					data[r][5] 		= fileMatch.getSpecificDataFromFile(fileMatch.getFileTwoAmountIndex(), FileMatch.FILE_TWO, firstDuplicated);
    					data[r+1][5] 	= fileMatch.getSpecificDataFromFile(fileMatch.getFileTwoAmountIndex(), FileMatch.FILE_TWO, secondDuplicated);
    				}
    				
    				dupRows++;
    			}    			
    			break;// end of duplicates
    		case PARTIAL_MATCH:
    			int partialRows	= fileMatch.getPartialMatchFile().size() ;    			
    			data 			= new Object[partialRows][6];
    			
    			for(int r = 0; r < partialRows; r++)
    			{    			
    				
					int firstPartial	= fileMatch.getPartialMatchFile().get(r)[0];
					int secondPartial 	= fileMatch.getPartialMatchFile().get(r)[1];
					//FIRST FILE
					//The date.
					data[r][0] 		= fileMatch.getSpecificDataFromFile(fileMatch.getFileOneDateIndex(), FileMatch.FILE_ONE, firstPartial);					
					//The reference.
					data[r][1] 		= fileMatch.getSpecificDataFromFile(fileMatch.getIndexTransactionFileOne(),FileMatch.FILE_ONE, firstPartial);
					//The amount.
					data[r][2] 		= fileMatch.getSpecificDataFromFile(fileMatch.getFileOneAmountIndex(), FileMatch.FILE_ONE, firstPartial);
					//SECOND FILE
					//The date.
					data[r][3] 		= fileMatch.getSpecificDataFromFile(fileMatch.getFileTwoDateIndex(), FileMatch.FILE_TWO, secondPartial);					
					//The reference.
					data[r][4] 		= fileMatch.getSpecificDataFromFile(fileMatch.getIndexTransactionFileTwo(), FileMatch.FILE_TWO, secondPartial);					
					//The amount.
					data[r][5] 		= fileMatch.getSpecificDataFromFile(fileMatch.getFileTwoAmountIndex(), FileMatch.FILE_TWO, secondPartial);					
				
    			}    			
    			break;// end of Partial Matches.
    		case NO_MATCH:
    			int noMatchFO	= fileMatch.getNoMatchesFileOne().size() ;
    			int noMatchFT	= fileMatch.getNoMatchesFileTwo().size() ;
    			rows 			= ( noMatchFO > noMatchFT ) ? noMatchFO : noMatchFT;
    			data 			= new Object[rows][6];    			
    			
    			for(int r = 0; r < rows; r++)
    			{    			
    				if(r < noMatchFO)
    				{
    					int firstNoMatch = fileMatch.getNoMatchesFileOne().get(r);
    					
    					//The date.
    					data[r][0] 		= fileMatch.getSpecificDataFromFile(fileMatch.getFileOneDateIndex(), FileMatch.FILE_ONE, firstNoMatch);    					
    					//The reference.
    					data[r][1] 		= fileMatch.getSpecificDataFromFile(fileMatch.getIndexTransactionFileOne(),FileMatch.FILE_ONE, firstNoMatch);    					
    					//The amount.
    					data[r][2] 		= fileMatch.getSpecificDataFromFile(fileMatch.getFileOneAmountIndex(), FileMatch.FILE_ONE, firstNoMatch);
    					
    				}
    				
    				if(r < noMatchFT)
    				{
    					int secondNoMatch = fileMatch.getNoMatchesFileTwo().get(r);
    					
    					//The date.
    					data[r][3] 		= fileMatch.getSpecificDataFromFile(fileMatch.getFileTwoDateIndex(), FileMatch.FILE_TWO, secondNoMatch);    					
    					//The reference.
    					data[r][4] 		= fileMatch.getSpecificDataFromFile(fileMatch.getIndexTransactionFileTwo(), FileMatch.FILE_TWO, secondNoMatch);    					
    					//The amount.
    					data[r][5] 		= fileMatch.getSpecificDataFromFile(fileMatch.getFileTwoAmountIndex(), FileMatch.FILE_TWO, secondNoMatch);
    					
    				}
    			}    			
    			break;// end of No Matches
    		case PARTIAL_DTAMT:
    			int dtAmtRows	= fileMatch.getMatchesByDateAmt().size();    			
    			data 			= new Object[dtAmtRows][6];
    			
    			for(int r = 0; r < dtAmtRows; r++)
    			{    			
    				
					int firstPartial	= fileMatch.getMatchesByDateAmt().get(r)[0];
					int secondPartial 	= fileMatch.getMatchesByDateAmt().get(r)[1];
					//FIRST FILE
					//The date.
					data[r][0] 		= fileMatch.getSpecificDataFromFile(fileMatch.getFileOneDateIndex(), FileMatch.FILE_ONE, firstPartial);					
					//The reference.
					data[r][1] 		= fileMatch.getSpecificDataFromFile(fileMatch.getIndexTransactionFileOne(),FileMatch.FILE_ONE, firstPartial);
					//The amount.
					data[r][2] 		= fileMatch.getSpecificDataFromFile(fileMatch.getFileOneAmountIndex(), FileMatch.FILE_ONE, firstPartial);
					//SECOND FILE
					//The date.
					data[r][3] 		= fileMatch.getSpecificDataFromFile(fileMatch.getFileTwoDateIndex(), FileMatch.FILE_TWO, secondPartial);					
					//The reference.
					data[r][4] 		= fileMatch.getSpecificDataFromFile(fileMatch.getIndexTransactionFileTwo(), FileMatch.FILE_TWO, secondPartial);					
					//The amount.
					data[r][5] 		= fileMatch.getSpecificDataFromFile(fileMatch.getFileTwoAmountIndex(), FileMatch.FILE_TWO, secondPartial);					
				
    			}    			
    			break;// end of Partial Matches By Date and Amount.
    	} // end of switch
    	
    }
    
    /**
     * Before calling this method buildData method should be called first.
     * The purpose is create the table and set the headers and data ready to be show.
     */
    private void buildTable() 
    {
    	String[] columnNames = {"Date File One ","Reference File One ","Amount File One ","Date File Two ","Reference File Two","Amount File Two"};
    	 
 
        final JTable table = new JTable(data, columnNames);
        table.setPreferredScrollableViewportSize(new Dimension(800, 100));
        table.setFillsViewportHeight(true);
 
        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);
 
        //Add the scroll pane to this panel.
        add(scrollPane);
    }
 
 
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame(reportType);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
 
        //Create and set up the content pane.       
        this.setOpaque(true); //content panes must be opaque
        frame.setContentPane(this);
 
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
    
    public final static String 	DUPLICATES 		= "Duplicates";
    public final static String 	NO_MATCH 		= "No Matches";
    public final static String 	PARTIAL_MATCH 	= "Partial Matches By Transaction";
    public final static String 	PARTIAL_DTAMT 	= "Partial Matches By Date & Amt";
        
	private String 						reportType;
	private FileMatch 					fileMatch;
	private Object[][] 					data;
 
}