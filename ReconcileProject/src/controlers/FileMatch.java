package controlers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import javax.swing.JTextArea;

/**
 * This class will perform all the logic to try to reconcile 2 files with CSV extension.
 * It will perform the comparision using first the Transaction ID, then the Amount and Date as second
 * decision choices.
 * It will define duplicates, perfect matches, no matches, partial match by transaction id and partial
 * match by date and amount.
 * @author Arturo Reyes
 *
 */
public class FileMatch {
	
	/**
	 * Main Constructor.
	 * It will set the initial status of the matching.
	 * 
	 */
	public FileMatch()
	{
		this.fileOne 		= null;
		this.fileTwo 		= null;
		this.log	 		= null;
		htHeadersFileOne 	= new Hashtable<>();
		htHeadersFileTwo 	= new Hashtable<>();
		htTrasactionsFileOne= new Hashtable<>();
		htTrasactionsFileTwo= new Hashtable<>();
		duplicatesFileOne	= new ArrayList<>();
		duplicatesFileTwo	= new ArrayList<>();
		htNoMatchFileOne	= new Hashtable<>();
		partialMatchFile	= new ArrayList<>();	
		matchByDateAmt		= new ArrayList<>();
		
	}
	
	
	/**
	 * This method will set the 2 files to match.
	 * @param fileOne - The first file of the Matching
	 * @param fileTwo - Second file for Matching
	 */
	public void setFiles(File fileOne, File fileTwo)
	{
		this.fileOne = fileOne;
		this.fileTwo = fileTwo;	
	}
	
	/**
	 * Method to set the JTextArea
	 * @param log - It will receive a JTextArea, where debugging output can be shown.
	 */
	public void setLogArea(JTextArea log)
	{
		this.log = log;
	}
	
	/**
	 * Enable/Disable the output of the matching process/debugging. 
	 * @param enableLog - Boolean true/false.
	 */
	public void enableLog(boolean enableLog)
	{
		this.enableLog = enableLog;
	}
	
	/**
	 * Utility method to print to textArea. 
	 * @param txt - The text to show.
	 */
	public void printToLog(String txt)
	{
		if(enableLog && log != null)
			log.append(txt);
	}
	
	/**
	 * Method that will do the reconciling process, first it will try by transaction
	 * and after by date and amount. 
	 * @return boolean - true if the process was successful or false, if it fail at any point.
	 */
	public boolean matchFiles()
	{
		
		printToLog("Starting compare..." + newline);
		dataFileOne = parse(fileOne,FILE_ONE);
		dataFileTwo = parse(fileTwo,FILE_TWO);
		//The first approach is to match by transactionType
		//so we need to get the position of each column.
		
		try {
			fileOneTrasactionIndex = htHeadersFileOne.get("TransactionID");
			fileTwoTrasactionIndex = htHeadersFileTwo.get("TransactionID");
		}catch(Exception e)
		{
			printToLog("Not able to match the files, no headers found or TransactionID is set "+  newline);
			return false;
		}
		
		boolean compareSuccesful = compareByTransactionId();	
		boolean compareByDateAmt = compareByDateAmt();
		
		printToLog("Compare ended..." + newline);
		return compareSuccesful && compareByDateAmt;		
	}
	
	/**
	 * It will compare the transactions of file one versus the transactions of file two
	 * by retrieving it using the transaction ID, then comparing the date and amount.
	 * @return true if comparison process was successful of false if not.
	 */
	private boolean compareByTransactionId()
	{
		//Getting all the unique transactions of File one.
		Set<String> fileOneKys = htTrasactionsFileOne.keySet();		
		
		try
		{
			//Setting the Date and Amount indexes for each file.
			fileOneDateIndex = htHeadersFileOne.get("TransactionDate");
			fileTwoDateIndex = htHeadersFileTwo.get("TransactionDate");
			
			fileOneAmountIndex = htHeadersFileOne.get("TransactionAmount");
			fileTwoAmountIndex = htHeadersFileTwo.get("TransactionAmount");
			
		} catch(Exception e)
		{
			printToLog("Not able to match the files, expected headers not found "+  newline);
			return false;
		}
		
		for(String k : fileOneKys)
		{	//Checking if the transaction exist on file two.
			if(htTrasactionsFileTwo.containsKey(k)) {
				//Compare their date, retrieving the info from each file using
				//the hash tables, with this we avoid traversing it.
				int indexFileOne = htTrasactionsFileOne.get(k);
				int indexFileTwo = htTrasactionsFileTwo.get(k);
				String fileOneDate = dataFileOne.get(indexFileOne).get(fileOneDateIndex);
				String fileTwoDate = dataFileTwo.get(indexFileTwo).get(fileTwoDateIndex);
				String fileOneAmt = dataFileOne.get(indexFileOne).get(fileOneAmountIndex);
				String fileTwoAmt = dataFileTwo.get(indexFileTwo).get(fileTwoAmountIndex);
				//If transaction ID, Date and Amount are the same is a perfect match, regardless of the other attributes
				if(fileOneDate.equalsIgnoreCase(fileTwoDate) && fileOneAmt.equalsIgnoreCase(fileTwoAmt))
				{
					perfectMatch++;
				} else
				{
					//If it wasnt a perfect match, it needs to be reported.
					partialMatch++;
					Integer[] partial = new Integer[] {indexFileOne,indexFileTwo};
					partialMatchFile.add(partial);
					printToLog("Coincidence in TransacId : "+ k + " " +fileOneDate +" vs " + fileTwoDate + " | "+ fileOneAmt +" vs "+ fileTwoAmt + newline);
				}
				//Remove from file two, the remaining records will be those that aren't on file one.
				htTrasactionsFileTwo.remove(k);
			
			}
			else
			{
				noMatch++;			
				htNoMatchFileOne.put(k,htTrasactionsFileOne.get(k));
			}			
		}
		printToLog("Remaining transactions: File one:"+ htNoMatchFileOne.size() + " File Two: " +htTrasactionsFileTwo.size() + newline);
		return true;		
	}
	
	/**This method has the second criteria to determine a match, if they have same date and amount
	 * they are partial match, and they need to be reviewed.
	 * @return true if comparison was successful.
	 */
	private boolean compareByDateAmt()
	{
		List<Integer> noMatchOne = getNoMatchesFileOne();
		List<Integer> noMatchTwo = getNoMatchesFileTwo();
		
		for(int i = 0; i < noMatchOne.size(); i++)
		{
			int indexFileOne = noMatchOne.get(i);
			//Get the date of File One No match
			String fileOneDate = dataFileOne.get(indexFileOne).get(fileOneDateIndex);
			String fileOneAmt  = dataFileOne.get(indexFileOne).get(fileOneAmountIndex);
			String fileOneTransaction = dataFileOne.get(indexFileOne).get(fileOneTrasactionIndex);
			
			//Compare against records in no match file two
			for(int j = 0; j < noMatchTwo.size(); j++)
			{
				int indexFileTwo = noMatchTwo.get(j);
				//Get the date of File One No match
				String fileTwoDate = dataFileTwo.get(indexFileTwo).get(fileTwoDateIndex);
				String fileTwoAmt  = dataFileTwo.get(indexFileTwo).get(fileTwoAmountIndex);
				String fileTwoTransaction  = dataFileTwo.get(indexFileTwo).get(fileTwoTrasactionIndex);
				
				if(fileOneDate.equalsIgnoreCase(fileTwoDate) && fileOneAmt.equalsIgnoreCase(fileTwoAmt))
				{					
					Integer[] noMatchDA = new Integer[] {indexFileOne,indexFileTwo};
					matchByDateAmt.add(noMatchDA);
					
					//Remove from the tables of noMatches to avoid duplicates.
					htNoMatchFileOne.remove(fileOneTransaction);
					noMatch--;
					htTrasactionsFileTwo.remove(fileTwoTransaction);
					
				}				
			}
		}
		
		return true;
	}
	
	/**
	 * Set the Headers on their HashTable, in which the index is the descriptor and the integer 
	 * stores the column.
	 * @param fileNumber - Determine which file is processing 
	 * @param headers	- List of strings with the headers.
	 */
	private void setColumnHeaders(int fileNumber,List<String> headers )
	{
		Hashtable<String,Integer> ht = (fileNumber == FILE_ONE) ? htHeadersFileOne : htHeadersFileTwo;
		
		for(int i = 0; i < headers.size(); i++)
		{
			ht.put(headers.get(i),i);
		}
		
		return;
	}
	
	
	/**
	 * Utility method to get the data from file having the row and the column.
	 * @param column - we want to check
	 * @param file - the number of the file to review, 1 or 2.
	 * @param row - Number of the row to get the data from.
	 * @return - The data queried from the file.
	 */
	public String getSpecificDataFromFile(int column, int file, Integer row)
	{
		String value;
		if( file == FILE_ONE)
			value = dataFileOne.get(row).get(column);
		else
			value = dataFileTwo.get(row).get(column);
		
		return value;
	}
	
	/**
	 * It will read the file line per line and by doing so, it will also determine
	 * if duplicates exist by transaction id, saving those records, and will also 
	 * set the total rows.
	 * 
	 * @param file 	- File that will be read and parsed. 
	 * @param fileNumber - Number of file we are processing 1 or 2.
	 * @return List<List<String>> = It will return a list of lists containing the 
	 * data of the file.
	 */
	private List<List<String>>parse(File file,int fileNumber){
		List<List<String>> 			dataFile 		= new ArrayList<List<String>>();
		//Save the transaction id with their row on a hash table while parsing to make the access time
		//when searching more faster.
		Hashtable<String,Integer>	htTrasactions 	= (fileNumber == FILE_ONE) ? htTrasactionsFileOne : htTrasactionsFileTwo; 
		boolean isFirstLine 	= true;
		int indexTransactions 	= -1;
		int indexDescription 	= -1;		
		int rows 				= 0;
		int totalRows 			= 0;
		try {
			Scanner reader = new Scanner(file);
		while (reader.hasNextLine()) {
			//we assume that always the file contains the name of the
			//columns on the first row.
			String data = reader.nextLine();
			List<String> lineData = Arrays.asList(data.split(","));
			if(isFirstLine)
			{
				setColumnHeaders(fileNumber,lineData);
				indexTransactions 	= (fileNumber == FILE_ONE) ? htHeadersFileOne.get("TransactionID") : htHeadersFileTwo.get("TransactionID");
				indexDescription 	= (fileNumber == FILE_ONE) ? htHeadersFileOne.get("TransactionDescription") : htHeadersFileTwo.get("TransactionDescription");				
				isFirstLine = false;
			} else
			{	
				totalRows++;
				String transactionId = lineData.get(indexTransactions);
				if(htTrasactions.containsKey(transactionId))
				{
					int previousRow = htTrasactions.get(transactionId);
					//If it has the same Transaction Id, probably is a reversal. 
					String previousDesc = dataFile.get(previousRow).get(indexDescription);
					String currentDesc  = lineData.get(indexDescription);
					String tentativeId	= transactionId+"_"+currentDesc;
					
					//If the description of the transaction is different for instance 
					//DEDUCT vs REVERSAL we are referring to different type of transaction even if
					//they have same transaction id
					if(!previousDesc.equalsIgnoreCase(currentDesc) && !htTrasactions.containsKey(tentativeId) )
					{
						// This is not a duplicate is another type of transaction
						htTrasactions.put(tentativeId,rows);						
					} 
					else
					{					
						//duplicate transactions
						if(fileNumber == FILE_ONE) 
						{
							Integer[] dups = new Integer[] {rows,previousRow};
							duplicatesFileOne.add(dups);							
						}
						else
						{
							Integer[] dups = new Integer[] {rows,previousRow};
							duplicatesFileTwo.add(dups);							
						}
					}
				} else
				{
					htTrasactions.put(transactionId,rows);
				}
				
			}
			dataFile.add(rows,lineData);	
			rows++;
		  }
		  reader.close();

		  if(fileNumber == FILE_ONE)
			  totalRowsFileOne = totalRows;
		  else
			  totalRowsFileTwo = totalRows;
		
		} catch(FileNotFoundException e){
			printToLog("File was not found: " + file.getName() + newline);	
		}
		return dataFile;		
	}
	
	public int getTotalRowsFileOne() {
		return totalRowsFileOne;
	}


	public int getTotalRowsFileTwo() {
		return totalRowsFileTwo;
	}


	public Hashtable<String, Integer> getUnMatchtedTransactionOnFileTwo() {
		return htTrasactionsFileTwo;
	}


	public List<Integer[]> getDuplicatesFileOne() {
		return duplicatesFileOne;
	}


	public List<Integer[]> getDuplicatesFileTwo() {
		return duplicatesFileTwo;
	}


	public List<Integer[]> getPartialMatchFile() {
		return partialMatchFile;
	}
	
	public List<Integer[]> getMatchesByDateAmt() {
		return matchByDateAmt;
	}


	public int getPerfectMatch() {
		return perfectMatch;
	}


	public int getPartialMatch() {
		return partialMatch;
	}


	public int getNoMatch() {
		return noMatch;
	}
	
	public List<Integer[]>  getNoMatchByDateAmt()
	{
		return matchByDateAmt;
	}
	
	public List<Integer> getNoMatchesFileOne()
	{
		return new ArrayList<Integer>(htNoMatchFileOne.values());
	}
	
	public List<Integer> getNoMatchesFileTwo()
	{
		return new ArrayList<Integer>(htTrasactionsFileTwo.values());
	}
	
	public List<List<String>> getDataFileOne() {
		return dataFileOne;
	}


	public List<List<String>> getDataFileTwo() {
		return dataFileTwo;
	}
	
	public int getIndexTransactionFileOne() {
		return fileOneTrasactionIndex;
	}
	
	public int getIndexTransactionFileTwo() {
		return fileTwoTrasactionIndex;
	}
	
	public int getFileOneDateIndex() {
		return fileOneDateIndex;
	}


	public int getFileTwoDateIndex() {
		return fileTwoDateIndex;
	}


	public int getFileOneAmountIndex() {
		return fileOneAmountIndex;
	}


	public int getFileTwoAmountIndex() {
		return fileTwoAmountIndex;
	}
	
	
	private	File						fileOne;
	private File						fileTwo;
	static private final String 		newline = "\n";	
	private JTextArea 					log;
	//Hash tables used.
	private Hashtable<String,Integer>	htHeadersFileOne;
	private Hashtable<String,Integer>	htHeadersFileTwo;	
	private Hashtable<String,Integer>	htTrasactionsFileOne;
	private Hashtable<String,Integer>	htTrasactionsFileTwo;
	private Hashtable<String,Integer>	htNoMatchFileOne;
	//Lists
	private List<List<String>> 			dataFileOne;
	private List<List<String>> 			dataFileTwo;
	private List<Integer[]>				duplicatesFileOne ;
	private List<Integer[]>				duplicatesFileTwo;
	private List<Integer[]>				partialMatchFile;
	private List<Integer[]>				matchByDateAmt;
	//Ints
	private int 						totalRowsFileOne = 0;
	private int 						totalRowsFileTwo = 0;
	private int							perfectMatch	= 0;
	private int							partialMatch	= 0;
	private int							noMatch			= 0;	
	private boolean						enableLog		= false;
	private int 						fileOneTrasactionIndex;
	private int 						fileTwoTrasactionIndex;
	private int 						fileOneDateIndex;
	private int 						fileTwoDateIndex;
	private int 						fileOneAmountIndex;
	private int 						fileTwoAmountIndex;
	
	//Static variables
	public static final int				FILE_ONE = 1;
	public static final int				FILE_TWO = 2;
	

}
